import React, { useEffect, useState } from 'react';
import { CRCard } from '../../globalComponents/surfaces/CRCard';
import styles from './Home.module.scss';
import logo from '../../assets/logo.svg';
//Functionality
import { fetchCollectionList, getDevice } from './Home.func';
import { CRListing } from '../../globalComponents/layout/CRListing';

export const Home = (props) => {
  //State
  const [loading, setLoading] = useState(true);
  const [listings, setListings] = useState(['']);
  const [device, setDevice] = useState('server');
  console.log(loading);
  console.log(device);
  //Lifecycle
  useEffect(() => {
    getDevice(setDevice);
    fetchCollectionList(setListings, setLoading);
  }, []);
  return (
    <div className={styles.mainContainer}>
      <div className={styles.firstFoldBG} data-testid="first-fold">
        <div className={styles.logo}>
          <img src={logo} alt={'Design Fest'} className={styles.logo} />
        </div>
        <div className={styles.bannersContainer}>
          <div className={styles.bannerRow}>
            <CRCard.Store
              variant={'medium'}
              storeName={'Minty Lavender Design'}
              storeImage={
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/2808cd73dbc3f060f1f18fd05bdf9f75-thumbnail-560x560-90.JPG'
              }
              imgObjectFit={'contain'}
            />

            <CRCard.Store
              storeName={'El buen coyote'}
              storeImage={
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/e9fb03293d32aa0add8029a391c2060c-crop-c0-5__0-5-280x280-90.jpeg'
              }
              imgObjectFit={'contain'}
            />
          </div>
          <div className={styles.bannerRow}>
            <CRCard.Store
              storeName={'HISTOS'}
              storeImage={
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/c5c13fd79724487c57169617f952c352-crop-c0-5__0-5-280x280-90.jpg'
              }
              imgObjectFit={'contain'}
            />
            <CRCard.Store
              storeName={'NUSA'}
              storeImage={
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/a9feee6b2d36017b899a3d299a869894-crop-c0-5__0-5-280x280-90.jpg'
              }
              imgObjectFit={'contain'}
            />
            <CRCard.Store
              storeName={'MORANINIE'}
              storeImage={
                'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/de807f82f9aa3af3012fc0171bf8ff79-thumbnail-560x560-90.jpg'
              }
              imgObjectFit={'cover'}
            />
          </div>
        </div>
      </div>
      {listings.map(function (listing, i) {
        return (
          <CRListing
            key={i}
            variant={listing.type}
            items={listing.items}
            name={listing.name}
            globalLoading={loading}
            display={device}
          />
        );
      })}
    </div>
  );
};
