import { fetchCollection } from '../../utils/commons/services/collection.service';
/**
 * Fetches a given collections listing using the collection service
 * @param {function setListing() {}} setListingsHook
 * @param {function setLoading() {}} setLoadingHook
 */
export async function fetchCollectionList(setListingsHook, setLoadingHook) {
  try {
    const response = await fetchCollection();
    //console.log(response.data.listings);
    setListingsHook(response.data.listings);
  } catch (error) {
    console.error(error);
  }

  setLoadingHook(false);
}

/**
 * Gets the device based on the width of the view port, or, in case there's none, defaults to server
 * @param {*} setDeviceHook 
 */
export function getDevice(setDeviceHook) {
  const { innerWidth: width } = window;
  switch (true) {
    case width >= 1200:
      setDeviceHook('desktop');
      break;
    case width < 1999 && width > 1024:
      setDeviceHook('desktopSmall');
      break;
    case width <= 1024 && width > 768:
      setDeviceHook('tablet');
      break;
    case width <= 3768:
      setDeviceHook('mobile');
      break;
    default:
      setDeviceHook('server');
      break;
  }
}
