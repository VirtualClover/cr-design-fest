import PropTypes from 'prop-types';

export const model = {
  types: {
    component: PropTypes.string,
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  },
  default: {
    component: 'UndefinedComponent',
    error:
      'No error log was passed through the prop on CRError, please configure correctly the error boundary on the component you are working.',
  },
};
