import PropTypes from 'prop-types';

export const model = {
  types: {
    limit: PropTypes.number.isRequired,
    children: PropTypes.node.isRequired,
    variant: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    align: PropTypes.string.isRequired,
    title: PropTypes.string,
  },
  default: {
    limit: 280,
    children: 'SAMPLE TEXT',
    variant: 'paragraph',
    color: 'dark200',
    align: 'left',
  },
};
