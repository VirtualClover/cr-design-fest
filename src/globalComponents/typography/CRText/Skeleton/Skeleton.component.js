import React from 'react';
import { CRSkeleton } from '../../../layout/CRSkeleton';
import styles from '../CRText.module.scss';

export const Skeleton = (props) => {
  return (
    <CRSkeleton.Text textVariant={props.variant} className={styles.skeleton} />
  );
};
