/**
 * Trims the string according to a character limit, returning the trimmed string and an ellipsis.
 * @param {string} string The string to be trimmed
 * @param {number} limit The character limit
 * @param {function} setFinalStringHook
 */
export function trimStringToLimit(string, limit, setFinalStringHook) {
  if (string.length > limit) {
    setFinalStringHook(string.substring(0, limit) + '...');
  } else {
    setFinalStringHook(string);
  }
}
