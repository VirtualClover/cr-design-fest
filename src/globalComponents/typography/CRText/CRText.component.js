import React, { useEffect, useState } from 'react';
import styles from './CRText.module.scss';
import { model } from './CRText.model';
import { Skeleton } from './Skeleton/Skeleton.component';
import { trimStringToLimit } from './CRText.func';
/**
 * The main text component, automatically caps at 280 characters if not specified otherwise, it's variants are the {@link https://virtualclover.gitlab.io/mijo/master/?path=/story/%F0%9F%A7%B1-foundations-%F0%9F%93%9C-typography-%F0%9F%94%A0-text-styles--title|Text Styles}.
 * @param {*} props
 * @returns
 */
export const CRText = (props) => {
  const [finalString, setFinalString] = useState('');
//Lifecycle
  useEffect(() => {
    trimStringToLimit(props.children, props.limit, setFinalString);
  }, [props.children, props.limit]);

  const classConstructor = `${styles.base} ${styles[props.align]} ${
    styles[props.variant]
  } ${styles[props.color]} ${props.style}`;

  const textTag = {
    title: (
      <h1 key={props.variant} className={classConstructor} title={props.title}>
        {finalString}
      </h1>
    ),
    subtitle: (
      <h2 key={props.variant} className={classConstructor} title={props.title}>
        {finalString}
      </h2>
    ),
    subtitle2: (
      <h3 key={props.variant} className={classConstructor} title={props.title}>
        {finalString}
      </h3>
    ),
    subtitle3: (
      <h4 key={props.variant} className={classConstructor} title={props.title}>
        {finalString}
      </h4>
    ),
    bold: (
      <p key={props.variant} className={classConstructor} title={props.title}>
        {finalString}
      </p>
    ),
    paragraph: (
      <p key={props.variant} className={classConstructor} title={props.title}>
        {finalString}
      </p>
    ),
    caption: (
      <p key={props.variant} className={classConstructor} title={props.title}>
        {finalString}
      </p>
    ),
    micro: (
      <p key={props.variant} className={classConstructor} title={props.title}>
        {finalString}
      </p>
    ),
  };

  return textTag[props.variant];
};

CRText.Skeleton = Skeleton;

CRText.propTypes = model.types;
CRText.defaultProps = model.default;
