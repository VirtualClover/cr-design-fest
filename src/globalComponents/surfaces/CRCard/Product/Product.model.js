import PropTypes from 'prop-types';

export const model = {
  types: {
    productName: PropTypes.string.isRequired,
    storeName: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    href: PropTypes.node.isRequired,
    priceWoDiscount: PropTypes.string.isRequired,
    badges: PropTypes.array,
  },
  default: {
    productName: 'PRODUCT_NAME',
    storeName: 'STORE_NAME',
    price: '0.0',
    productImage:
      'https://virtualclover.gitlab.io/mijo/master/static/media/placeholder.e9b1f94e.jpg',
    href:
      'https://canastarosa.com',
    priceWoDiscount: '0.0',
    badges: [],
  },
};
