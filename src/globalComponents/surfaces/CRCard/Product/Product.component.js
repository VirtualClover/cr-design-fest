import React from 'react';
import baseStyle from '../CRCard.module.scss';
import styles from './Product.module.scss';
import { model } from './Product.model';
import { CRText } from '../../../typography/CRText';
import { CRImg } from '../../../media/CRImg';
/**
 * The product card, shows the image of the product, it's name, the store, and if it has promotions like discount and free shipping.
 * @param {*} props 
 * @returns 
 */
export const Product = (props) => {
  return (
    <a
      className={`${baseStyle.mainWrapper} ${styles.mainWrapper}`}
      href={props.href}
    >
      <CRImg
        className={styles.productImage}
        alt={props.productName}
        src={props.productImage}
      />
      <div className={styles.productDetail}>
        <div className={styles.namesWrapper}>
          <CRText limit={21} title={props.productName}>
            {props.productName}
          </CRText>
          <CRText limit={15} variant={'caption'} color={'dark100'}>
            {props.storeName}
          </CRText>
        </div>
        <div className={styles.badges}>
          {props.badges[0] && (
            <CRText variant={'caption'} color={'turquiose400'}>
              <b>
                ¡Aplicable para <br /> envío gratis!
              </b>
            </CRText>
          )}
        </div>
        <div className={styles.priceWrapper}>
          {props.price !== props.priceWoDiscount && (
            <CRText
              limit={90}
              variant={'caption'}
              color={'dark100'}
              style={styles.discountText}
            >
              ${props.priceWoDiscount} MXN
            </CRText>
          )}
          <CRText limit={90} variant={'bold'} color={'dark100'}>
            ${props.price} MXN
          </CRText>
        </div>
      </div>
    </a>
  );
};

Product.propTypes = model.types;
Product.defaultProps = model.default;
