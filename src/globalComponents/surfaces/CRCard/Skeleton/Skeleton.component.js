import React from 'react';
import styles from '../CRCard.module.scss';
import { CRSkeleton } from '../../../layout/CRSkeleton';
/**
 * The skelketon component of the product card
 * @param {*} props 
 * @returns 
 */
export const Skeleton = (props) => {
    return (
      <CRSkeleton className={styles.skeleton}/>
    );
  };