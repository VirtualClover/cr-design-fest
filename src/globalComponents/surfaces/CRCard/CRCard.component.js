import React from 'react';
import styles from './CRCard.module.scss';
import { model } from './CRCard.model';
import { Product } from './Product/Product.component';
import { Store } from './Store';
import { Skeleton } from './Skeleton/Skeleton.component';

/**
 * The base card component; a surface.
 * @param {*} props 
 * @returns The card component
 */
export const CRCard = (props) => {
  return (
    <a className={styles.mainWrapper} href={props.href}>
    {props.children}
    </a>
  );
};

CRCard.Product = Product;
CRCard.Store = Store;
CRCard.Skeleton = Skeleton;

CRCard.propTypes = model.types;
CRCard.defaultProps = model.default;
