import PropTypes from 'prop-types';

export const model = {
  types: {
    storeName: PropTypes.string.isRequired,
    storeImage: PropTypes.node.isRequired,
    variant: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
    imgObjectFit: PropTypes.string.isRequired,
    wrapperClassName: PropTypes.string
  },
  default: {
    storeName: 'STORE_NAME',
    storeImage:
      'https://virtualclover.gitlab.io/mijo/master/static/media/placeholder.e9b1f94e.jpg',
    variant: 'small',
    href: 'https://canastarosa.com/',
    imgObjectFit: 'cover',
  },
};
