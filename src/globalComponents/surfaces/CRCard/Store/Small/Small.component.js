import React from 'react';
import baseStyles from '../../CRCard.module.scss';
import styles from '../Store.module.scss';
import { model } from '../Store.model';
import { CRText } from '../../../../typography/CRText';
import { CRButton } from '../../../../inputs/CRButton';
import { CRImg } from '../../../../media/CRImg';

export const Small = (props) => {
  return (
    <a
      className={`${baseStyles.mainWrapper} ${styles.mainWrapper} ${styles.smallWrapper}`}
      href={props.href}
    >
      <div className={`${styles.cardDetail} ${styles.detailSmall}`}>
        <div className={styles.namesWrapper}>
          <CRText limit={29} variant={'subtitle3'}>
            {props.storeName}
          </CRText>
          <CRButton>Visitar tienda</CRButton>
        </div>
      </div>
      <CRImg
        className={`${styles.storeImageSmall} ${styles[props.imgObjectFit]}`}
        alt={props.storeName}
        src={props.storeImage}
        wrapperClassName={`${styles.imgWrapper} ${props.imgClassName}`}
      />
    </a>
  );
};

Small.propTypes = model.types;
Small.defaultProps = model.default;
