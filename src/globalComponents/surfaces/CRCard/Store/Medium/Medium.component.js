import React from 'react';
import baseStyles from '../../CRCard.module.scss';
import styles from '../Store.module.scss';
import { model } from '../Store.model';
import { CRText } from '../../../../typography/CRText';
import { CRButton } from '../../../../inputs/CRButton';
import { CRImg } from '../../../../media/CRImg';

export const Medium = (props) => {
  return (
    <a
      className={`${baseStyles.mainWrapper} ${styles.mainWrapper} ${styles.mediumWrapper}`}
      href={props.href}
    >
      <div className={styles.cardDetail}>
        <div className={styles.namesWrapper}>
          <CRText limit={29} variant={'subtitle2'}>
            {props.storeName}
          </CRText>
          <CRButton>Visitar tienda</CRButton>
        </div>
      </div>
      <CRImg
        className={`${styles.storeImageMedium}  ${styles[props.imgObjectFit]}`}
        alt={props.storeName}
        src={props.storeImage}
        wrapperClassName={styles.imgWrapper}
      />
    </a>
  );
};

Medium.propTypes = model.types;
Medium.defaultProps = model.default;
