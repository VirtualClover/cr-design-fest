import { Small } from './Small/Small.component';
import { Medium } from './Medium/Medium.component';
import { model } from './Store.model';
export const Store = (props) => {
  const variants = {
    small: <Small {...props} />,
    medium: <Medium {...props} />,
  };

  return variants[props.variant];
};

Store.propTypes = model.types;
Store.defaultProps = model.default;
