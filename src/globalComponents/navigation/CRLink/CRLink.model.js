import PropTypes from 'prop-types';

export const model = {
  types: {
    children: PropTypes.node,
    redirect: PropTypes.string.isRequired,
    target: PropTypes.string,
  },
  default: {
    children: 'EXAMPLE HYPERLINK',
    redirect: 'https://canastarosa.com/',
    target: '',
  },
};
