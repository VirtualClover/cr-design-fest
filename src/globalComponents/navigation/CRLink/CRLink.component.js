import React from 'react';
import styles from './CRLink.module.scss';
import { model } from './CRLink.model';
import { CRText } from '../../typography/CRText/CRText.component';
/**
 * The basic hyperlink component used for href.
 * @param {*} props
 * @returns A button component
 */
export const CRLink = (props) => {
  return (
    <a
      href={props.redirect}
      target={props.target}
      className={`${styles.wrapper} ${props.wrapperClassName}`}
    >
      <CRText variant={props.variant} color={'main300'}>
        {props.children}
      </CRText>
    </a>
  );
};

CRLink.propTypes = model.types;
CRLink.defaultProps = model.default;
