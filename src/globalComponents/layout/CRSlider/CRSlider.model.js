import PropTypes from 'prop-types';

export const model = {
  types: {
    children: PropTypes.node.isRequired,
    startFrom: PropTypes.number.isRequired,
    showNav: PropTypes.bool
  },
  default: {
    startFrom: 0,
    showNav: false
  },
};
