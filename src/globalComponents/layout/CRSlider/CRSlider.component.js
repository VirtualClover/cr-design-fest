import React, { useRef, useState, useEffect } from 'react';
import { CRButton } from '../../inputs/CRButton';
import {
  showNextSlide,
  showPrevSlide,
  updateNavElements,
} from './CRSlider.func';
import { model } from './CRSlider.model';
import styles from './CRSlider.module.scss';
/**
 * The slider component, creates an slider given CRSlides as children
 * @param {*} props
 * @returns
 * @see CRSlide
 */
export const CRSlider = (props) => {
  const slider = useRef();
  const [activeSlide, setActiveSlide] = useState(props.startFrom);
  //Lifecycle
  useEffect(() => {
    if (props.startFrom) {
      slider.current.style.transform = `translateX(${props.startFrom * -100}%)`;
    }
  }, [props.startFrom]);

  return (
    <div className={styles.sliderWrapper}>
      <div className={styles.slider} ref={slider}>
        {props.children}
      </div>

      <div className={styles.sliderWrapperButtons}>
        <CRButton
          onClick={() => showPrevSlide(activeSlide, slider, setActiveSlide)}
          disabled={activeSlide === 0}
        >
          Anterior
        </CRButton>
        <CRButton
          onClick={() =>
            showNextSlide(activeSlide, props.children, slider, setActiveSlide)
          }
          disabled={activeSlide === props.children.length - 1}
        >
          Siguiente
        </CRButton>
      </div>

      {props.showNav && (
        <div className={styles.sliderWrapperNav}>
          {props.children.map((s, i) => (
            <span
              key={i}
              className={activeSlide === i ? 'active' : ''}
              onClick={() => updateNavElements(i, slider)}
            ></span>
          ))}
        </div>
      )}
    </div>
  );
};


CRSlider.propTypes = model.types;
CRSlider.defaultProps = model.default;