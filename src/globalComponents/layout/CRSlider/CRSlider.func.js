export function showPrevSlide(activeSlide, slider, setActiveSlide) {
  if (activeSlide > 0) {
    slider.current.style.transform = `translateX(${(activeSlide - 1) * -100}%)`;
    setActiveSlide((prev) => prev - 1);
  }
}

export function showNextSlide(activeSlide, children, slider, setActiveSlide) {
  if (activeSlide < children.length - 1) {
    slider.current.style.transform = `translateX(${(activeSlide + 1) * -100}%)`;
    setActiveSlide((prev) => prev + 1);
  }
}

export function updateNavElements(index, slider, setActiveSlide) {
  setActiveSlide(index);
  slider.current.style.transform = `translateX(${index * -100}%)`;
}
