import PropTypes from 'prop-types';

export const model = {
  types: {
    children: PropTypes.node
  },
  default: {
  },
};
