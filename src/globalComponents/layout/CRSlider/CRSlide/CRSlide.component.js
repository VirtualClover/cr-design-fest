import React from 'react';
import styles from '../CRSlider.module.scss';
import {model} from './CRSlide.model';
/**
 * The child component fo CRSlider, needs to be nested in a CRSlide to work
 * @param {*} props 
 * @returns An Slide
 */
export const CRSlide = (props) => {
  return (
    <div className={styles.slideWrapper}>
      <div className={styles.slideContent}>{props.children}</div>
    </div>
  );
};

CRSlide.propTypes = model.types;
CRSlide.defaultProps = model.default;