import { render, screen } from '@testing-library/react';
import { CRSlider } from '.';
import { CRSlide } from './CRSlide/CRSlide.component';

test('component should render a slider', () => {
  render(
    <CRSlider>
      <CRSlide />
    </CRSlider>
  );
  const nextButton = screen.getByText('Siguiente');
  const prevButton = screen.getByText('Anterior');
});
