import React from 'react';
import styles from '../CRSkeleton.module.scss';
import { model } from '../CRSkeleton.model';

/**
 * The image variant of the skeleton component
 * @param {*} props
 * @returns
 */
export const Image = (props) => {
  return (
    <div
      className={`${styles.mainWrapper} ${props.className}`}
      data-testid={'image-skeleton'}
    >
      <img
        alt={'Cargando Imagen...'}
        src={props.imgSrc}
        className={`${props.className}`}
      />
    </div>
  );
};

Image.propTypes = model.types;
Image.defaultProps = model.default;
