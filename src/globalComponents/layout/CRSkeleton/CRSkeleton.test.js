import { render, screen } from '@testing-library/react';
import { CRSkeleton } from '.';

test('component should render block skeleton by default', () => {
  render(<CRSkeleton />);
  const block = screen.getByTestId('block-skeleton');
});

test('component should render image variant', () => {
  render(<CRSkeleton.Image />);
  const image = screen.getByTestId('image-skeleton');
});

test('component should render text variant', () => {
  render(<CRSkeleton.Text />);
  const text = screen.getByTestId('text-skeleton');
});
