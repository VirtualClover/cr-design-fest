import { Block } from './Block/Block.component';
import { Image } from './Image/Image.component';
import { Text } from './Text/Text.component';

export const CRSkeleton = Block;

CRSkeleton.Image = Image;
CRSkeleton.Text = Text;
CRSkeleton.Block = Block;
