import PropTypes from 'prop-types';

export const model = {
  types: {
    textVariant: PropTypes.string.isRequired,
    className: PropTypes.string,
    imgSrc: PropTypes.node.isRequired,
  },
  default: {
    textVariant: 'paragraph',
    imgSrc:
      'https://virtualclover.gitlab.io/mijo/master/static/media/placeholder.e9b1f94e.jpg',
  },
};
