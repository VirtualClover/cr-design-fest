import React from 'react';
import styles from '../CRSkeleton.module.scss';
import { model } from '../CRSkeleton.model';

/**
 * The block variant of the skeleton component
 * @param {*} props
 * @returns
 */
export const Block = (props) => {
  return (
    <div
      className={`${styles.mainWrapper} ${props.className}`}
      data-testid={'block-skeleton'}
    />
  );
};

Block.propTypes = model.types;
Block.defaultProps = model.default;
