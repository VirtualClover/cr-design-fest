import React from 'react';
import styles from '../CRSkeleton.module.scss';
import { model } from '../CRSkeleton.model';
/**
 * The text variant of the skeleton component, its variants are the same as the {@link https://virtualclover.gitlab.io/mijo/master/?path=/story/%F0%9F%A7%B1-foundations-%F0%9F%93%9C-typography-%F0%9F%94%A0-text-styles--title|Text Styles}.
 * @param {*} props 
 * @returns 
 */
export const Text = (props) => {
  return (
    <div
      className={`${styles.mainWrapper} ${styles.baseText} ${styles[props.textVariant]} ${
        props.className
      }`
    }
    data-testid={'text-skeleton'}
    />
  );
};

Text.propTypes = model.types;
Text.defaultProps = model.default;
