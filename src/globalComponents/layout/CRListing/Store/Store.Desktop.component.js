import React, { useEffect, useState } from 'react';
import { CRCard } from '../../../surfaces/CRCard';
import { CRText } from '../../../typography/CRText';
import { CRSlide, CRSlider } from '../../CRSlider';
import styles from '../CRListing.module.scss';
import { Skeleton } from '../Skeleton/Skeleton.component';
import { generateListing } from '../Product/Product.func';

/**
 * The component of a loaded list.
 * @param {*} props
 * @returns
 */
const LoadedList = (props) => {
  return (
    <div className={styles.listingContainer}>
      <div className={styles.sectionTitle}>
        <CRText variant={'subtitle'} color={'brown400'}>
          {props.name}
        </CRText>
      </div>
      <CRSlider>
        {props.slides.map(function (slide, i) {
          return (
            <CRSlide key={i}>
              <div className={styles.productsContainer}>
                {slide.map(function (item, i) {
                  return (
                    <CRCard.Store
                      key={i}
                      storeName={item.store.name}
                      storeImage={item.store.photo.medium}
                      href={item.store.public_link}
                      imgClassName={styles.storeImage}
                      imgObjectFit={'contain'}
                    />
                  );
                })}
              </div>
            </CRSlide>
          );
        })}
      </CRSlider>
    </div>
  );
};

/**
 *  The desktop component for product listings
 * @param {*} props
 * @returns
 */
export const Desktop = (props) => {
  //Local state
  const [slides, setSlides] = useState([]);
  const [renderedComponent, setRenderedComponent] = useState(
    <Skeleton noSlides={7} />
  );
  //Lifecycle
  useEffect(() => {
    console.log(props.items);
    props.items.length > 0 && generateListing(props.items, 3, setSlides);
  }, [props]);

  useEffect(() => {
    slides.length > 0 &&
      setRenderedComponent(<LoadedList slides={slides} {...props} />);
  }, [props, slides]);

  return renderedComponent;
};

/* Slide structure
        <CRSlide>
          <div className={styles.productsContainer}>
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
          </div>
        </CRSlide>


*/
