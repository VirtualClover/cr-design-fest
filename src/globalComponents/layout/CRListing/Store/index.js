import { Desktop } from './Store.Desktop.component';
import { model } from '../CRListing.model';
/**
 * The Store variant of the listing, contains their media variants(Desktop, mobile)
 * @param {*} props 
 * @returns 
 */
export const Store = (props) => {
  const variants = {
    desktop: <Desktop {...props}/>,
    tablet: <Desktop {...props} />,
    mobile: <Desktop {...props} />,
    server: <Desktop {...props} />,
    
  };

  return variants[props.display];
};

Store.propTypes = model.types;
Store.defaultProps = model.default;
