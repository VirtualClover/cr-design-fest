import PropTypes from 'prop-types';

export const model = {
  types: {
    display: PropTypes.string.isRequired,
    variant: PropTypes.string.isRequired,
    items: PropTypes.array.isRequired,
    name: PropTypes.string.isRequired,
  },
  default: {
    display: 'desktop',
    variant: 'product',
    items: [],
    name: 'Productos super chidos'
  },
};
