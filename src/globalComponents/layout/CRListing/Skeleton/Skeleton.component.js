import React, { useEffect, useState } from 'react';
import { CRCard } from '../../../surfaces/CRCard';
import { CRText } from '../../../typography/CRText';
import styles from '../CRListing.module.scss';
/**
 * The skeleton of a listing
 * @param {*} props 
 * @returns 
 */
export const Skeleton = (props) => {
  const [rows, setRows] = useState([]);

  //Lifecycle
  useEffect(() => {
    for (var i = 1; i <= props.noSlides; i++) {
      setRows((rows) => [...rows, <CRCard.Skeleton key={rows.length + 1} />]);
    }
  }, [props.noSlides]);

  return (
    <div className={styles.listingContainer}>
      <div className={styles.sectionTitle}>
        <CRText.Skeleton variant={'subtitle'} />
      </div>
      <div className={styles.productsContainer}>{rows}</div>
    </div>
  );
};
