import React, { useEffect, useState } from 'react';
import { Product } from './Product';
import { Store } from './Store';
import { model } from './CRListing.model';
/**
 * The listing component contains the product and store listings, each one with their media variants(Desktop, mobile)
 * @param {*} props 
 * @returns A listing
 */
export const CRListing = (props) => {
  const [variant, setVariant] = useState(props.variant);
  const variants = {
    product: <Product {...props} />,
    store: null,
    empty: null,
  };
//Lifecycle
  useEffect(() => {
    !props.items[0] && !props.globalLoading && setVariant('empty');
  }, [props.items, props.globalLoading]);

  return variants[variant];
};

CRListing.propTypes = model.types;
CRListing.defaultProps = model.default;
