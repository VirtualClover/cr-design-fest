import { render, screen } from '@testing-library/react';
import { CRListing } from '.';

const items = [
  {
    id: 46680,
    order: 1,
    photo: {
      small: '',
      original: '',
      medium: '',
      big: '',
    },
    product: {
      id: 203693,
      badges: [],
      description:
        'Nuez extraída del Anacardium Occidentale. Su composición de minerales como el potasio, magnesio, fósforo, zinc y vitaminas B-9, K, B-3, entre otros, la convierten en una de las nueces más ricas, nutritivas y saludables. Su contenido de triptófano y fitoesteroles ayudan a combatir la depresión, controlar los triglicéridos y mantener el óptimo nivel de colesterol.\r\nSu presentación sin tostar y sin sal es en definitiva, un alimento indispensable en tu dieta.',
      discount: '0.00',
      excerpt: '',
      mean_product_score: 0.0,
      name: 'Nuez de la India sin sal 250g',
      photo: {
        original:
          'https://canastarosa.s3.amazonaws.com/media/market/product/8be5264fe352295be482e7603ffd8794.jpg',
        small:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/8be5264fe352295be482e7603ffd8794-crop-c0-5__0-5-280x280-90.jpg',
        big:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/8be5264fe352295be482e7603ffd8794-thumbnail-1120x1120-90.jpg',
        medium:
          'https://canastarosa.s3.amazonaws.com/media/__sized__/market/product/8be5264fe352295be482e7603ffd8794-thumbnail-560x560-90.jpg',
      },
      public_link:
        'https://canastarosa.com/stores/gramo-al-grano/products/nuez-de-la-india-sin-sal-250g-11990010',
      price: '170.00',
      price_without_discount: '170.00',
      slug: 'nuez-de-la-india-sin-sal-250g-11990010',
      store: {
        name: 'Gramo al Grano',
        slug: 'gramo-al-grano',
      },
      physical_properties: {
        fabrication_time: 0,
        is_available: {
          value: 1,
          display_name: 'Disponible en Stock',
        },
        life_time: 0,
        minimum_shipping_date: '2021-04-07',
        maximum_shipping_date: '2021-07-12',
        size: {
          value: 1,
          display_name: 'Sobre',
        },
        weight: '0.255',
        shipping_methods: ['express-moto'],
      },
    },
  },
];

test('should show listing given an array', () => {
  render(<CRListing items={items} />);
  const title = screen.getByText('Productos super chidos');
});
