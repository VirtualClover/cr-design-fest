/**
 * Generates the slides given an array of products
 * @param {Array} itemArray - An array fo items
 * @param {number} itemsPerRow - The items that will be displayed on a row
 * @param {function} setSlidesHook
 * @param {function} setLoadingHook
 */
export async function generateListing(itemArray, itemsPerRow, setSlidesHook) {
  try {
    var array = [],
      size = itemsPerRow;
      // Duplicate the array so we don't splice the original 
    var duplicatedArray = [...itemArray];
    while (duplicatedArray.length > 0) array.push(duplicatedArray.splice(0, size));
    setSlidesHook(array);
    // console.log(props);
  } catch (error) {
    console.error(error);
  }
}
