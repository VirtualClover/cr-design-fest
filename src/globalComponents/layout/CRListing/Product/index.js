import { Desktop } from './Product.Desktop.component';
import { model } from '../CRListing.model';
import { Mobile } from './Product.Mobile.component';
/**
 * The product variant of the listing, contains their media variants(Desktop, mobile)
 * @param {*} props 
 * @returns 
 */
export const Product = (props) => {
  const variants = {
    desktop: <Desktop {...props}/>,
    tablet: <Mobile {...props} />,
    mobile: <Mobile {...props} />,
    server: <Desktop {...props} />,
    
  };

  return variants[props.display];
};

Product.propTypes = model.types;
Product.defaultProps = model.default;
