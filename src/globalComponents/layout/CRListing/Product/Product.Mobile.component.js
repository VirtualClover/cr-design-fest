import React, { useEffect, useState } from 'react';
import { CRCard } from '../../../surfaces/CRCard';
import { CRText } from '../../../typography/CRText';
import styles from '../CRListing.module.scss';
import { Skeleton } from '../Skeleton/Skeleton.component';

/**
 * The component of a loaded list.
 * @param {*} props 
 * @returns 
 */
const LoadedList = (props) => {
  return (
    <div className={styles.listingContainer}>
      <div className={styles.sectionTitle}>
        <CRText variant={'subtitle'} color={'brown400'}>
          {props.name}
        </CRText>
      </div>
      <div className={styles.scrollWrapper}>
      {props.items.map(function (item, i) {
        return (
          <CRCard.Product
            key={i}
            productName={item.product.name}
            storeName={item.product.store.name}
            productImage={item.product.photo.medium}
            price={item.product.price}
            href={item.product.public_link}
            priceWoDiscount={item.product.price_without_discount}
            badges={item.product.badges}
          />
        );
      })}
      </div>
    </div>
  );
};

/**
 *  The mobile component for product listings
 * @param {*} props
 * @returns
 */

export const Mobile = (props) => {
  //Local state
  const [renderedComponent, setRenderedComponent] = useState(
    <Skeleton noSlides={2} />
  );
  //Lifecycle
  useEffect(() => {
    props.items.length > 0 &&
      setRenderedComponent(<LoadedList {...props}/>);
  }, [props]);

  return renderedComponent;
};

/* Slide structure
        <CRSlide>
          <div className={styles.productsContainer}>
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
            <CRCard productName={'Ramo de rosas'} storeName={'Amore'} />
          </div>
        </CRSlide>


*/
