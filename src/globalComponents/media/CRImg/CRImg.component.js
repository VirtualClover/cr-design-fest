import React, { useEffect, useState } from 'react';
import styles from './CRImg.module.scss';
import { model } from './CRImg.model';
import { CRSkeleton } from '../../layout/CRSkeleton';
import { handleOnLoad, useFallbackImg } from './CRImg.func';
/**
 * An image component, with loading skeletons and customizale placeholders in case of errors.
 * @param {*} props
 * @returns
 */
export const CRImg = (props) => {
  //Local state
  const [src, setSrc] = useState(props.src);
  const [loading, setLoading] = useState(true);
  const srcProps = useFallbackImg(src, setSrc, props.placeHolder);
  //Lifecycle
  useEffect(() => {
    setSrc(props.src);
  }, [props.src]);

  return (
    <div
      className={`${styles.imgWrapper} ${
        props.wrapperClassName ? props.wrapperClassName : props.className
      }`}
    >
      <img
        className={`${styles.base} ${props.className} ${
          loading && styles.notLoaded
        }`}
        onLoad={() => handleOnLoad(setLoading)}
        {...srcProps}
        alt={props.alt}
      />
      {loading && <CRSkeleton.Image className={props.className} />}
    </div>
  );
};

CRImg.propTypes = model.types;
CRImg.defaultProps = model.default;
