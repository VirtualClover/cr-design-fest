/**
 * Handles the end of the image loading, just updates the loading state.
 * @param {*} setLoadingHook 
 */
export function handleOnLoad(setLoadingHook) {
  setLoadingHook(false);
}
/**
 * Handler for when we need to use the palceholder
 * @param {*} src The source of the image
 * @param {*} fallback The placeholder
 * @returns The props of src and on Error to be propagated to a component
 */
export function useFallbackImg(src, setSrcHook, fallback) {
  function onError(error) {
    // React bails out of hook renders if the state
    // is the same as the previous state, otherwise
    // fallback erroring out would cause an infinite loop
    setSrcHook(fallback);
    console.error(error);
  }

  return { src, onError };
}
