import PropTypes from 'prop-types';

export const model = {
  types: {
    src: PropTypes.node.isRequired,
    placeHolder: PropTypes.node.isRequired,
    className: PropTypes.string,
    alt: PropTypes.string,
  },
  default: {
    src:
      'https://virtualclover.gitlab.io/mijo/master/static/media/placeholder.e9b1f94e.jpg',
      placeHolder:
      'https://virtualclover.gitlab.io/mijo/master/static/media/placeholder.e9b1f94e.jpg',
      alt: 'Imagen'
  },
};
