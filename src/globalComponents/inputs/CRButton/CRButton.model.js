import PropTypes from 'prop-types';

export const model = {
  types: {
    href: PropTypes.string,
    children: PropTypes.string.isRequired,
    onclick: PropTypes.func,
    disabled: PropTypes.bool.isRequired,
  },
  default: {
    children: 'BUTTON',
    disabled: false,
  },
};
