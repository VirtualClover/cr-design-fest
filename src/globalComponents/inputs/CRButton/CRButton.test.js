import { fireEvent, render, screen } from '@testing-library/react';
import { CRButton } from './CRButton.component';

test('component should render a button', () => {
  render(<CRButton />);
  const nextButton = screen.getByText('BUTTON');
});

test('component should excecute function at onClick', () => {
  const handleClick =  jest.fn();
  render(<CRButton onClick={handleClick} />);
  fireEvent.click(screen.getByText('BUTTON'));
  expect(handleClick).toHaveBeenCalledTimes(1);
});
