import React from 'react';
import styles from './CRButton.module.scss';
import { model } from './CRButton.model';
import { CRText } from '../../typography/CRText/CRText.component';
/**
 * The basic button component used for href.
 * @param {*} props
 * @returns A button component
 */
export const CRButton = (props) => {
  return (
    <a
      className={`${styles.mainWrapper} ${props.disabled && styles.disabled}`}
      href={props.href}
      onClick={props.onClick}
    >
      <CRText limit={20} variant={'bold'} color={'white'} style={styles.text}>
        {props.children}
      </CRText>
    </a>
  );
};

CRButton.propTypes = model.types;
CRButton.defaultProps = model.default;
