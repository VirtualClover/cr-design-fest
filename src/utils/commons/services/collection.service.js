const axios = require('axios');
/**
 * Fetches a specific collection from the CR API. 
 * @returns the payload of the collection
 */
export async function fetchCollection() {
  const url =
    'https://canastarosa.com/services/api/v1/marketing/landing-page/productos-eco/';
  const axiosConfig = {
    method: 'get',
    url: url,
  };
  try {
    const res = await axios(axiosConfig);
    const response = {
      data: res.data,
    };
    return response;
  } catch (error) {
    console.error(error);
  }
}
